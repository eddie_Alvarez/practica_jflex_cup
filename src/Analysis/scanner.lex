package Analysis;
import java_cup.runtime.Symbol;
import Bean.ErrorGr;
import java.util.ArrayList; 
%% 

%{
    /*Area de declaración de codigo java. 
    *Funciones y variables necesarias
    *
    */
    ArrayList<ErrorGr> listError = new ArrayList<ErrorGr>();
    String nombre;
    public void imprimir(String cadena)
    {
    	System.out.println(cadena);
    }
%}

%cupsym sym 
%class scanner
%cup
%public
%unicode
%line
%column
%char
%ignorecase
%8bit
%full
%unicode


Commentary = "//" [^\r\n]* [^\r\n]
MultilineComment = "/*" [^/] ~"*/" | "/*" "/"+ "*/"
Number = [0-9]+
Chain = [\"] [^\"\n]* [\"\n]
Letter = [a-zA-ZÑñ]+
id = {Letter}({Letter}|{Number}|"_")*
Space = \t|\f|" "|\r|\n

%%

{Space} {}
{Number} {return new Symbol(sym.Number, yycolumn, yyline, yytext());}
{Chain} {return new Symbol(sym.Chain, yycolumn, yyline, yytext());}
"definirglobales" {return new Symbol(sym.definirglobales, yycolumn, yyline, yytext());}
"int" {return new Symbol(sym.pint, yycolumn, yyline, yytext());}
"string" {return new Symbol(sym.string, yycolumn, yyline, yytext());}
"titulo" {return new Symbol(sym.titulo, yycolumn, yyline, yytext());}
"puntosxy" {return new Symbol(sym.puntosxy, yycolumn, yyline, yytext());}
"graficabarras" {return new Symbol(sym.graficabarras, yycolumn, yyline, yytext());}
"titulox" {return new Symbol(sym.titulox, yycolumn, yyline, yytext());}
"tituloy" {return new Symbol(sym.tituloy, yycolumn, yyline, yytext());}
"ejex" {return new Symbol(sym.ejex, yycolumn, yyline, yytext());}
"ejey" {return new Symbol(sym.ejey, yycolumn, yyline, yytext());}
"graficalineas" {return new Symbol(sym.graficalineas, yycolumn, yyline, yytext());}
"definirxyline" {return new Symbol(sym.definirxyline, yycolumn, yyline, yytext());}
"nombre" {return new Symbol(sym.nombre, yycolumn, yyline, yytext());}
"color" {return new Symbol(sym.color, yycolumn, yyline, yytext());}
"grosor" {return new Symbol(sym.grosor, yycolumn, yyline, yytext());}
"puntos" {return new Symbol(sym.puntos, yycolumn, yyline, yytext());}
"galeria" {return new Symbol(sym.galeria, yycolumn, yyline, yytext());}
"id" {return new Symbol(sym.pid, yycolumn, yyline, yytext());} 
{id} {return new Symbol(sym.id, yycolumn, yyline, yytext());}
";"  {return new Symbol(sym.punto_coma, yycolumn, yyline, yytext());}
"="  {return new Symbol(sym.igual, yycolumn, yyline, yytext());}
"/"  {return new Symbol(sym.division, yycolumn, yyline, yytext());}
"*"  {return new Symbol(sym.multiplicacion, yycolumn, yyline, yytext());}
"{"  {return new Symbol(sym.llave_abierta, yycolumn, yyline, yytext());}
"}"  {return new Symbol(sym.llave_cerrada, yycolumn, yyline, yytext());}
":"  {return new Symbol(sym.dos_puntos, yycolumn, yyline, yytext());}
"["  {return new Symbol(sym.corchete_abierto, yycolumn, yyline, yytext());}
"]"  {return new Symbol(sym.corchete_cerrado, yycolumn, yyline, yytext());}
"+"  {return new Symbol(sym.signo_mas, yycolumn, yyline, yytext());}
"("  {return new Symbol(sym.parentesis_abierto, yycolumn, yyline, yytext());}
")"  {return new Symbol(sym.parentesis_cerrado, yycolumn, yyline, yytext());}
","  {return new Symbol(sym.coma, yycolumn, yyline, yytext());}
"¿"  {return new Symbol(sym.abre_interrogacion, yycolumn, yyline, yytext());}
"?"  {return new Symbol(sym.cierra_interrogacion, yycolumn, yyline, yytext());}
"-"  {return new Symbol(sym.signo_menos, yycolumn, yyline, yytext());}
{Commentary} {}
{MultilineComment} {}

.
{
    System.err.println("Error lexico: " + yytext() + " Linea: " + (yyline+1) + " Columna: " + (yycolumn+1));
    listError.add(new ErrorGr(yytext(), (yyline+1), (yycolumn+1), "Simbolo desconocido"));
}