/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analysis;

/**
 *
 * @author Eddie
 */
public class Variable {
    public String id;
    public String type;
    public String value;
    public int valueNumber;
    
    public Variable(String id, String type, String value) {
        this.id = id;
        this.type = type;
        this.value = value;
    }

    public Variable(String id, String type, int valueNumber) {
        this.id = id;
        this.type = type;
        this.valueNumber = valueNumber;
    }
    
  
}
