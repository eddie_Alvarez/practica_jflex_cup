/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.ArrayList;

/**
 *
 * @author Eddie
 */
public class GraphicLines implements Graphic{
    
    private String id;
    private String title;
    private String titleX;
    private String titleY;
    private ArrayList<XYFeatures> listXY;

    public GraphicLines() {
        this.listXY = new ArrayList<XYFeatures>();
    }
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleX() {
        return titleX;
    }

    public void setTitleX(String titleX) {
        this.titleX = titleX;
    }

    public String getTitleY() {
        return titleY;
    }

    public void setTitleY(String titleY) {
        this.titleY = titleY;
    }

    public ArrayList<XYFeatures> getListXY() {
        return listXY;
    }

    public void setListXY(ArrayList<XYFeatures> listXY) {
        this.listXY = listXY;
    }
    
    public void insertList(XYFeatures item){
        listXY.add(item);
    }
    
    
    public boolean Verify(GraphicLines g){
        XYFeatures yy = new XYFeatures();
         if(g.id.equals("") || g.id == null){
            return true;
        }
        if(g.title.equals("") || g.title == null){
            return true;
        }
        if(g.titleX.equals("") || g.titleX == null){
            return true;
        }
        if(g.titleY.equals("") || g.titleY == null){
            return true;
        }
        if(g.listXY == null || g.listXY.isEmpty()){
            return true;
        }
        for(XYFeatures f: g.listXY){
            if(yy.Verify(f)){
                return true;
            }   
        }
        return false;
    }
    
}
