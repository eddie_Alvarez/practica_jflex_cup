/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.Hashtable;

/**
 *
 * @author Eddie
 */
public class XYFeatures {
    
    private String name;
    private String color;
    private int thickness;
    private Hashtable<Integer, Integer> listPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getThickness() {
        return thickness;
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public Hashtable<Integer, Integer> getListPoints() {
        return listPoints;
    }

    public void setListPoints(Hashtable<Integer, Integer> listPoints) {
        this.listPoints = listPoints;
    }
    
    public boolean Verify(XYFeatures g){
        if(g.name.equals("") || g.name == null){
            return true;
        }
        if(g.color.equals("") || g.color == null){
            return true;
        }
        if(g.listPoints.isEmpty() || g.listPoints == null){
            return true;
        }
        return false;
    }
    
}
