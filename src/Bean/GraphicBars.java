/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 *
 * @author Eddie
 */
public class GraphicBars implements Graphic{
    
    private String id;
    private String title;
    private ArrayList<String> listXaxis;
    private String titleX;
    private ArrayList<Integer> listYaxis;
    private String titleY;
    private Hashtable<Integer, Integer> listXY;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getListXaxis() {
        return listXaxis;
    }

    public void setListXaxis(ArrayList<String> listXaxis) {
        this.listXaxis = listXaxis;
    }

    public String getTitleX() {
        return titleX;
    }

    public void setTitleX(String titleX) {
        this.titleX = titleX;
    }

    public ArrayList<Integer> getListYaxis() {
        return listYaxis;
    }

    public void setListYaxis(ArrayList<Integer> listYaxis) {
        this.listYaxis = listYaxis;
    }

    public String getTitleY() {
        return titleY;
    }

    public void setTitleY(String titleY) {
        this.titleY = titleY;
    }

    public Hashtable<Integer, Integer> getListXY() {
        return listXY;
    }

    public void setListXY(Hashtable<Integer, Integer> listXY) {
        this.listXY = listXY;
    }
    
    public boolean Verify(GraphicBars g){
        if(g.id.equals("") || g.id == null){
            return true;
        }
        if(g.title.equals("") || g.title == null){
            return true;
        }
        if(g.titleX.equals("") || g.titleX == null){
            return true;
        }
        if(g.titleY.equals("") || g.titleY == null){
            return true;
        }
        if(g.listXaxis == null || g.listXaxis.isEmpty()){
            return true;
        }
        if(g.listYaxis == null || g.listYaxis.isEmpty()){
            return true;
        }
        if(g.listXY == null || g.listXY.isEmpty()){
            return true;
        }
        return false;
    }
    
}
